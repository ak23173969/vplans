package com.wanpeng.workspace.vplans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication(scanBasePackages = {"com.wanpeng.workspace.vplans"})
@MapperScan(basePackages={"com.wanpeng.workspace.vplans.mapper"})
public class VplansApplication {

    public static void main(String[] args) {
        SpringApplication.run(VplansApplication.class, args);
    }

}
