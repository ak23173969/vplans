package com.wanpeng.workspace.vplans.utils;

import java.util.Arrays;

/**
 * 
 * @author: wanpeng
 * @date:   2019年2月26日 下午4:22:46   
 *     
 * @Copyright: 2019 http://www.hotcomm.net All rights reserved.
 */
public class ArrayUtils {
	
	/**
	 *  数组截取部分
	 * @param source
	 * @param beginIndex
	 * @param endIndex
	 * @param length
	 * @return 
	 */
	public static byte [] substring(byte [] source,int beginIndex,int endIndex,int length) {
		byte [] result = new byte [length] ;
		int count = 0;
		for(int index = beginIndex;index<=endIndex;index++) {
			result[count] = source[index];
			count ++;
		}
		return result;
	}
	
	public static void main(String[] args) {
		byte [] b = new byte [] {1,1,-1,-1,-1,0,1,0,1};
		byte [] k = ArrayUtils.substring(b, 2, 4, 3);
		System.out.println(Arrays.toString(k));
	}
}
