package com.wanpeng.workspace.vplans.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ByteUtils {

    public static byte [] converArray(Collection<Byte> collections){
        if(collections==null){
            return null;
        }
        byte [] array = new byte [collections.size()];
        int index = 0;
        for (Iterator<Byte> iterator = collections.iterator();iterator.hasNext();) {
            array[index] = iterator.next();
            index++;
        }
        return array;
    }
    
    public static List<Byte> converList(byte [] array){
    	List<Byte> list = new ArrayList<>(array.length);
    	for(byte b:array) {
    		list.add(b);
    	}
    	return list;
    }
    
}
