package com.wanpeng.workspace.vplans.service;

import com.wanpeng.workspace.vplans.mapper.HomePlanMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class HomeService {

    @Autowired
    HomePlanMapper homePlanMapper;


    public List<Map<String,Object>> getCurrenYeahTasks(Integer userId,Integer spaceId){
        List<Map<String,Object>>  data =  homePlanMapper.getCurrenYeahTasks(userId, spaceId);
        return data;
    }

    @PostConstruct
    public void initTest(){
        try {
            log.info(getCurrenYeahTasks(1,2).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
