package com.wanpeng.workspace.vplans.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "time_plans")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class TimePlan {

    @Id
    @Column(name = "id")
    private Integer id;

    private String title;
    private Integer types;
    private Date create_time;
    private Date end_time;
    private Integer space_id;

    public TimePlan(String title, Integer types, Date create_time, Date endTime, Integer space_id) {
        this.title = title;
        this.types = types;
        this.create_time = create_time;
        this.end_time = end_time;
        this.space_id = space_id;
    }
}
