package com.wanpeng.workspace.vplans.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "plance_space")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class HomeSpace {

    @Id
    @Column(name = "member_id")
    private Integer id;

    private String title;
}
