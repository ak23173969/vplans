package com.wanpeng.workspace.vplans.controller;

import com.wanpeng.workspace.vplans.mapper.HomePlanMapper;
import com.wanpeng.workspace.vplans.model.TimePlan;
import com.wanpeng.workspace.vplans.utils.SpringUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.PostConstruct;
import java.util.Date;

@Controller
@RequestMapping("/plan")
@Log4j2
public class HomeController {

    @Autowired
    private HomePlanMapper homePlanMapper;

    /**
     *  主空间内容展示
     * @param model
     * @return
     */
    @RequestMapping("/home")
    public String themaleaf(Model model) {
        model.addAttribute("name", "Joshua");
        TimePlan plan = new TimePlan("00001",1,new Date(),new Date(),1);
        homePlanMapper.insert(plan);
        return "home";
    }

    /**
     *  主空间内容展示
     * @param model
     * @return
     */
    @RequestMapping("/head")
    public String main(Model model) {
        return "head";
    }
    
}
