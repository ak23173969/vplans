package com.wanpeng.workspace.vplans.mapper;

import com.wanpeng.workspace.vplans.model.HomeSpace;
import tk.mybatis.mapper.common.BaseMapper;

public interface HomeSpaceMapper extends BaseMapper<HomeSpace> {
}
