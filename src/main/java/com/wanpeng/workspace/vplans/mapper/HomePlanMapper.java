package com.wanpeng.workspace.vplans.mapper;

import com.wanpeng.workspace.vplans.model.TimePlan;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;
import java.util.Map;

public interface HomePlanMapper extends BaseMapper<TimePlan> {

    /**
     *
     * @param userId
     * @param spaceId
     * @return
     */
    List<Map<String,Object>> getCurrenYeahTasks(@Param("userId") Integer userId, @Param("spaceId") Integer spaceId);


}
